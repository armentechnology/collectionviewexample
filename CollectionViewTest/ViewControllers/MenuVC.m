//
//  ViewController.m
//  CollectionViewTest
//
//  Created by Jose Catala on 28/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//


#import "MenuVC.h"
#import "DataAux.h"
#import "TestCollectionViewCell.h"

@interface MenuVC () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    MenuDataAux *dataModel;
    NSMutableArray *dataArray;
}

@property (nonatomic, weak) IBOutlet UICollectionView *myCollectionView;

@end

@implementation MenuVC

#pragma mark LIFECYCLE

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    dataModel = [[MenuDataAux alloc]init];
    dataArray = [[NSMutableArray alloc] initWithArray:[dataModel topMenuarray]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    dataModel = nil;
    _delegate = nil;
}

#pragma mark - UICollectionViewDatasource

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return dataArray.count;
}

#pragma mark - UICollectionViewDelegate

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TestCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    
    MenuDataAux *data = [dataArray objectAtIndex:indexPath.row];
    
    cell.lblTitle.text = data.title;
    
    cell.lblTitle.font = [UIFont fontWithName:cell.lblTitle.font.fontName size:data.size];
    
    if (data.active)
    {
        cell.selectedMark.backgroundColor = [UIColor colorWithRed:12/255.0f green:132/255.0f blue:0/255.0f alpha:1];
        cell.lblTitle.textColor = [UIColor colorWithRed:12/255.0f green:132/255.0f blue:0/255.0f alpha:1];;
    }
    else
    {
        cell.selectedMark.backgroundColor = _myCollectionView.backgroundColor;
        cell.lblTitle.textColor = [UIColor blackColor];
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    MenuDataAux *data = [dataArray objectAtIndex:indexPath.row];
    
    [dataArray enumerateObjectsUsingBlock:^(MenuDataAux* obj, NSUInteger idx, BOOL * _Nonnull stop) {
        obj.active = NO;
    }];
    
    data.active = YES;
    
    [_myCollectionView reloadData];
    
    if ([_delegate respondsToSelector:@selector(menuDidTappedOnOption:)])
    {
        [_delegate menuDidTappedOnOption:indexPath.row];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = self.myCollectionView.frame.size.width / dataArray.count;
    
    return CGSizeMake(width, self.myCollectionView.frame.size.height);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

- (CGFloat) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}

@end
