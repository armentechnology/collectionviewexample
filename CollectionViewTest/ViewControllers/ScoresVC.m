//
//  ScoresVC.m
//  CollectionViewTest
//
//  Created by Jose Catala on 29/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "ScoresVC.h"
#import "ScoresCollectionViewCell.h"
#import "ScoresTipsCollectionViewCell.h"
#import "TipsVC.h"
#import "DataAux.h"

@interface ScoresVC () <UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate>
{
    ScoresDataAux *scoresData;
    NSMutableArray *dataArray;
}

@property (nonatomic, weak) IBOutlet UICollectionView *myCollectionView;

@property NSInteger indexToMove;

@end

@implementation ScoresVC

#pragma mark LIFECYCLE

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    scoresData = [[ScoresDataAux alloc] init];
    
    dataArray = [[NSMutableArray alloc] initWithArray:[scoresData contentArrayForType:0]];
    
    self.indexToMove = 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updatePossitionNotice:) name:@"ScoreTabBarDidTapped" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateContentNotice:) name:@"MenuTabBarDidTapped" object:nil];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
    
    [self animateNeedleForIndexPath:path];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
}

#pragma mark - UICollectionViewDatasource

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return dataArray.count;
}

#pragma mark - UICollectionViewDelegate

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    id cell = nil;
    
    if (indexPath.row == dataArray.count - 1)
    {
        cell = (ScoresTipsCollectionViewCell *)[self returnTipsCellForIndexPath:indexPath];
    }
    else
    {
        cell = (ScoresCollectionViewCell *)[self returnScoresCellForIndexPath:indexPath];
    }
    
    return cell;
}

- (ScoresTipsCollectionViewCell *) returnTipsCellForIndexPath:(NSIndexPath *)indexPath
{
    ScoresTipsCollectionViewCell *cell =  [_myCollectionView dequeueReusableCellWithReuseIdentifier:@"ScoresTipsCell" forIndexPath:indexPath];
    
    ScoresDataAux *score = [dataArray objectAtIndex:indexPath.row];
    
    TipsVC *vc = [[TipsVC alloc] initWithData:score];
    
    [self addChildViewController:vc];
    [cell.contentView addSubview:vc.view];
    vc.view.frame = CGRectMake(0, 0, cell.contentView.frame.size.width, cell.contentView.frame.size.height);
    [vc didMoveToParentViewController:self];
    
    vc = nil;
            
    return cell;
}

- (ScoresCollectionViewCell *) returnScoresCellForIndexPath:(NSIndexPath*)indexPath
{
    ScoresCollectionViewCell *cell = [_myCollectionView dequeueReusableCellWithReuseIdentifier:@"ScoresCell" forIndexPath:indexPath];
    
    ScoresDataAux *score = [dataArray objectAtIndex:indexPath.row];
    
    cell.lblScoreTitle.text = score.title;
    
    cell.degrees = score.degrees;
    
    cell.lblScoreValue.text = score.value;
    
    cell.lblScoreValue.backgroundColor = score.color;
    
    cell.imgNeedle.image = [cell.imgNeedle.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [cell.imgNeedle setTintColor:score.color];
    
    [cell animateNeedle];

    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = self.myCollectionView.frame.size.width*0.8;

    return CGSizeMake(width, self.myCollectionView.frame.size.height);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 20;
}

#pragma mark UISCROLLVIEW DELEGATE

/*
 * Used to center the collection view cell without using paging
 * https://stackoverflow.com/questions/29658328/uicollectionview-horizontal-paging-not-centered
 */
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    //Ensure the scrollview is the collectionview we care about
    if (scrollView == self.myCollectionView)
    {
        // Find cell closest to the frame centre with reference from the targetContentOffset.
        CGPoint frameCentre = self.myCollectionView.center;
        CGPoint targetOffsetToCentre = CGPointMake((* targetContentOffset).x + frameCentre.x, (* targetContentOffset).y + frameCentre.y);
        
        NSIndexPath *indexPath = [self.myCollectionView indexPathForItemAtPoint:targetOffsetToCentre];
        
        //Check for "edgecase" that the target will land between cells and then find a close neighbour to prevent scrolling to index {0,0}.
        while (!indexPath) {
            targetOffsetToCentre.x += ((UICollectionViewFlowLayout *)self.myCollectionView.collectionViewLayout).minimumInteritemSpacing;
            indexPath = [self.myCollectionView indexPathForItemAtPoint:targetOffsetToCentre];
        }
        
        // Find the centre of the target cell
        CGPoint centreCellPoint = [self.myCollectionView layoutAttributesForItemAtIndexPath:indexPath].center;
        
        // Calculate the desired scrollview offset with reference to desired target cell centre.
        CGPoint desiredOffset = CGPointMake(centreCellPoint.x - frameCentre.x, centreCellPoint.y - frameCentre.y);
        *targetContentOffset = desiredOffset;
        
        NSIndexPath *path = [_myCollectionView indexPathForItemAtPoint:centreCellPoint];
        
        if ([_delegate respondsToSelector:@selector(scoresDidMoveToIndex:)])
        {
            [_delegate scoresDidMoveToIndex:path.row];
        }
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int indexOfPage = scrollView.contentOffset.x / scrollView.frame.size.width;

    NSIndexPath *path = [NSIndexPath indexPathForRow:indexOfPage+1 inSection:0];
    
    [self animateNeedleForIndexPath:path];
}

- (void) animateNeedleForIndexPath:(NSIndexPath *)path
{
    ScoresCollectionViewCell *cell = (ScoresCollectionViewCell*)[self.myCollectionView cellForItemAtIndexPath: path];
    
    if ([cell respondsToSelector:@selector(animateNeedle)])
    {
        cell.imgNeedle.transform = CGAffineTransformMakeRotation(0);
        [cell animateNeedle];
    }
}

#pragma mark NOTIFICATIONS

- (void) updatePossitionNotice:(NSNotification*)notice
{
    NSDictionary *dic = [notice userInfo];
    
    NSInteger index = [[dic objectForKey:@"index"] integerValue];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
    
    [self.myCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredVertically | UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    
    [self animateNeedleForIndexPath:indexPath];
}

- (void) updateContentNotice:(NSNotification*)notice
{
    NSDictionary *dic = [notice userInfo];
    
    NSInteger index = [[dic objectForKey:@"index"] integerValue];
    //NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
    
    dataArray = [[NSMutableArray alloc] initWithArray:[scoresData contentArrayForType:index]];
    
    
    [self.myCollectionView reloadData];
    [self animateNeedleForIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

@end
