//
//  MainVC.m
//  CollectionViewTest
//
//  Created by Jose Catala on 28/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "MainVC.h"
#import "MenuVC.h"
#import "ScoresVC.h"
#import "DataAux.h"

@interface MainVC () <TopMenuDelegate, ScoresVCDelegate,UITabBarDelegate>

@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UIView *scoresView;
@property (weak, nonatomic) IBOutlet UITabBar *myTabBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation MainVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Use the delegate methods as a self method to mark the first items as selected
    [self scoresDidMoveToIndex:0];
    [self menuDidTappedOnOption:0];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    MenuVC *menuVC = [st instantiateViewControllerWithIdentifier:@"MenuVC"];
    
    menuVC.delegate = self;
    
    [self addChildViewController:menuVC];
    [self.menuView addSubview:menuVC.view];
    menuVC.view.frame = CGRectMake(0, 0, self.menuView.frame.size.width, self.menuView.frame.size.height);
    [menuVC didMoveToParentViewController:self];
    
    menuVC = nil;
    
    ScoresVC *scoresVC = [st instantiateViewControllerWithIdentifier:@"ScoresVC"];
    
    scoresVC.delegate = self;
    
    [self addChildViewController:scoresVC];
    [self.scoresView addSubview:scoresVC.view];
    scoresVC.view.frame = CGRectMake(0, 0, self.scoresView.frame.size.width, self.scoresView.frame.size.height);
    [scoresVC didMoveToParentViewController:self];
    
    scoresVC = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark UITABBAR DELEGATE

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    NSInteger index = [tabBar.items indexOfObject:item];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ScoreTabBarDidTapped" object:nil userInfo:@{@"index":[NSNumber numberWithInteger:index]}];
}

#pragma mark TOPMENU DELEGATE

- (void) menuDidTappedOnOption:(NSInteger)option
{
    //Do the appropiate job here
    ScoresDataAux *data = [[ScoresDataAux alloc] init];
    
    NSArray *auxArray = [data contentArrayForType:option];
    
    data = [auxArray objectAtIndex:option];
    
    self.lblTitle.text = data.title;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"MenuTabBarDidTapped" object:nil userInfo:@{@"index":[NSNumber numberWithInteger:option]}];
}

#pragma mark SCORESVC DELEGATE

- (void) scoresDidMoveToIndex:(NSInteger)index
{
    [self.myTabBar setSelectedItem:[_myTabBar.items objectAtIndex:index]];
}

@end
