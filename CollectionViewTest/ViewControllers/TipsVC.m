//
//  TipsVC.m
//  CollectionViewTest
//
//  Created by Jose Catala on 29/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "TipsVC.h"
#import "ScoresTableTextCell.h"
#import "ScoresTablePictureCell.h"
#import "DataAux.h"

@interface TipsVC () <UITableViewDelegate, UITableViewDataSource>
{
    TipsDataAux *tips;
    NSMutableArray *mArray;
}

@property (weak, nonatomic) IBOutlet UITableView *myTableView;

@end

@implementation TipsVC


- (id) initWithData:(id)data
{
    UIStoryboard *st = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    TipsVC *vc = [st instantiateViewControllerWithIdentifier:@"TipsVC"];
    
    self = vc;
    
    if (self)
    {
        tips = [[TipsDataAux alloc] init];
        mArray = [[NSMutableArray alloc] initWithArray:[tips contentArray]];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.myTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITABLEVIEW DATASOURCE

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return mArray.count;
}

#pragma mark UITABLEVIEW DELEGATE

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id cell;
    
    if (indexPath.row == 0)
    {
        cell = [self returnTextCellForIndexPath:indexPath];
    }
    else
    {
        cell = [self returnPicturesCellForIndexPath:indexPath];
    }
    
    return cell;
}

- (ScoresTableTextCell *) returnTextCellForIndexPath:(NSIndexPath *)indexPath
{
    ScoresTableTextCell *cell = [_myTableView dequeueReusableCellWithIdentifier:@"textCell" forIndexPath:indexPath];
    
    tips = [mArray objectAtIndex:indexPath.row];
    
    cell.text.text = tips.text;
    
    return cell;
}

- (ScoresTablePictureCell *) returnPicturesCellForIndexPath:(NSIndexPath *)indexPath
{
    ScoresTablePictureCell *cell = [_myTableView dequeueReusableCellWithIdentifier:@"picturesCell" forIndexPath:indexPath];
    
    tips = [mArray objectAtIndex:indexPath.row];
    
    cell.text.text = tips.text;
    
    cell.image.image = [UIImage imageNamed:tips.imgName];
    
    return cell;
}

#pragma mark UITABLEVIEW DELEGATE

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}


@end
