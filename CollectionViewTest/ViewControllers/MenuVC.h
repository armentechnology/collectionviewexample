//
//  ViewController.h
//  CollectionViewTest
//
//  Created by Jose Catala on 28/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TopMenuDelegate <NSObject>

-(void)menuDidTappedOnOption:(NSInteger)option;

@end

@interface MenuVC : UIViewController

@property (nonatomic, weak) id<TopMenuDelegate>delegate;

@end

