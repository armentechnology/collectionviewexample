//
//  ScoresVC.h
//  CollectionViewTest
//
//  Created by Jose Catala on 29/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ScoresVCDelegate <NSObject>

-(void)scoresDidMoveToIndex:(NSInteger)index;

@end

@interface ScoresVC : UIViewController

@property (nonatomic, weak) id<ScoresVCDelegate>delegate;

@end
