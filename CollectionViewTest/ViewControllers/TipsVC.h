//
//  TipsVC.h
//  CollectionViewTest
//
//  Created by Jose Catala on 29/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TipsVC : UIViewController

- (id) initWithData:(id)data;

@end
