//
//  ScoresCollectionViewCell.m
//  CollectionViewTest
//
//  Created by Jose Catala on 29/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "ScoresCollectionViewCell.h"

@implementation ScoresCollectionViewCell

@synthesize degrees = _degrees;

- (void) animateNeedle
{
    dispatch_async(dispatch_get_main_queue(), ^ {
        if(!UIAccessibilityIsReduceMotionEnabled())
        {
            [UIView animateWithDuration:1 delay:0.1 usingSpringWithDamping:0.2f initialSpringVelocity:0.45f options:UIViewAnimationOptionCurveEaseInOut animations:^{
                self.imgNeedle.layer.affineTransform = CGAffineTransformMakeRotation(self.degrees);
            } completion:nil];
        }
        else
        {
            self.imgNeedle.layer.affineTransform = CGAffineTransformMakeRotation(self.degrees);
        }
    });
}

@end
