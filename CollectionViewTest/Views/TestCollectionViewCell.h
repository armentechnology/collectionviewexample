//
//  TestCollectionViewCell.h
//  CollectionViewTest
//
//  Created by Jose Catala on 28/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TestCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *selectedMark;

@end
