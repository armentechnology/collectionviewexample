//
//  ScoresCollectionViewCell.h
//  CollectionViewTest
//
//  Created by Jose Catala on 29/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ScoresCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblScoreTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgNeedle;
@property (weak, nonatomic) IBOutlet UILabel *lblScoreValue;
@property float degrees;

- (void) animateNeedle;

@end
