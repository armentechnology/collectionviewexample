//
//  MTShadowedRoundedView.m
//  morethan
//
//  Created by Jose Catala on 08/08/2018.
//  Copyright © 2018 Trak. All rights reserved.
//

#import "MTShadowedRoundedView.h"

@implementation MTShadowedRoundedView


- (void)drawRect:(CGRect)rect
{
    /* set the corner */
    self.layer.cornerRadius = 5.0f;
    self.layer.masksToBounds = YES;
    
    /* create and configure the shadowView */
    UIView *shadowView = [UIView new];
    shadowView.backgroundColor = self.backgroundColor; // just to make shadow visible
    shadowView.layer.cornerRadius = 5.0f;
    shadowView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    shadowView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
    shadowView.layer.shadowOpacity = 0.5f;
    shadowView.layer.shadowRadius = 3.f;
    
    /* put shadowView into superview right below self */
    [self.superview insertSubview:shadowView belowSubview:self];
    
    /* set shadowView's frame equal to self */
    shadowView.frame = self.frame;
}

@end
