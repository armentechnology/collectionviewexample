//
//  CustomTabBar.m
//  tabTest
//
//  Created by Jose Catala on 29/08/2018.
//  Copyright © 2018 Jose Catala. All rights reserved.
//

#import "CustomTabBar.h"

@implementation CustomTabBar

- (void) awakeFromNib
{
    [super awakeFromNib];
    
    /** create the background image for the tabbar, this is done in runtime
     *  so we dont need to create new assets if adding or removing buttons
     **/
    self.selectionIndicatorImage = [self imageFromColor:[UIColor whiteColor]];
    
    [self.items enumerateObjectsUsingBlock:^(UITabBarItem * _Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
        item.image = [item.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    }];
    
    self.barTintColor = [UIColor colorWithRed:0/255.0f green:59/255.0f blue:31/255.0f alpha:1];
}

#pragma mark UTILS

-(UIImage *)imageFromColor:(UIColor *)color
{
    NSInteger width = self.bounds.size.width / [self.items count];
    CGRect rect = CGRectMake(0, 0, width, self.frame.size.height);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

@end
