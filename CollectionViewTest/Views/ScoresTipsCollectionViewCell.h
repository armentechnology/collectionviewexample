//
//  ScoresTipsCollectionViewCell.h
//  CollectionViewTest
//
//  Created by Jose Catala on 29/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoresTipsCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) IBOutlet UIView *embededView;
@end
