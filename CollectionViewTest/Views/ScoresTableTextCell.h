//
//  ScoresTableTextCell.h
//  CollectionViewTest
//
//  Created by Jose Catala on 30/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoresTableTextCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *text;

@end
