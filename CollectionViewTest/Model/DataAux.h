//
//  DataAux.h
//  CollectionViewTest
//
//  Created by Jose Catala on 30/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataAux : NSObject

@end

@interface MenuDataAux : NSObject

@property (nonatomic, copy) NSString *title;
@property BOOL active;
@property NSInteger size;

- (NSMutableArray *) topMenuarray;

@end

@interface ScoresDataAux : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *value;
@property (nonatomic, copy) id color;
@property float degrees;

- (NSMutableArray *) contentArrayForType:(NSInteger)type;

@end

@interface TipsDataAux : NSObject

@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *imgName;

- (NSMutableArray *) contentArray;

@end

