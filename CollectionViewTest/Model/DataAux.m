//
//  DataAux.m
//  CollectionViewTest
//
//  Created by Jose Catala on 30/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import "DataAux.h"
#import <UIKit/UIKit.h>

#define kNumberOfSteps  20.0f
#define kAngleTotal     270.0f

@implementation DataAux

@end

@implementation MenuDataAux

-(NSMutableArray *)topMenuarray
{
    NSArray *array = @[@"Overall",@"Speed",@"Smoothness",@"Usage"];
    NSMutableArray *mArray = [[NSMutableArray alloc]init];
    
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        @autoreleasepool
        {
            MenuDataAux *data = [MenuDataAux new];
            data.title = array[idx];
            if (idx == 0) {
                data.active = 1;
            }
            else
            {
                data.active = 0;
            }
            data.size = 12;
            [mArray addObject:data];
        }
    }];
    
    return mArray;
}
@end

@implementation ScoresDataAux

- (NSMutableArray *) contentArrayForType:(NSInteger)type;
{
    NSArray *array = @[@"For this quarter",
                                        @"For the last day you drove",
                                        @"For the last 7 days",
                                        @"For this policy year to date",
                                        @"Explaining your driving score"];
    
    NSArray *arrayTexts;
    
    NSArray *mScores;
    
    switch (type)
    {
        case 0:
           arrayTexts = @[@"Improving your usage score will help to reduce your chances of being in an accident, and increase your rewards. See our tips for improving your usage score.",
                @"Your %@ score for the last day you drove didn’t quite reach 5. How about setting yourself a target of scoring above 5 for the rest of the week?",
                @"Your %@ score on the last day you drove was below 0. How about setting yourself a target of scoring above 5 for the rest of the week to increase your chances of earning rewards?",
                @"Explaining your driving score",
                @"Your %@ score on the last day you drove was below 0. How about setting yourself a target of scoring above 5 for the rest of the week to increase your chances of earning rewards?"];
            mScores = @[@(3),@(3.2),@(-5.7),@(3.4),@(0.0)];
            break;
        case 1:
            arrayTexts = @[@"Improving your usage score will help to reduce your chances of being in an accident, and increase your rewards. See our tips for improving your usage score.",
                           @"Your %@ score for the last day you drove didn’t quite reach 5. How about setting yourself a target of scoring above 5 for the rest of the week?",
                           @"Your %@ score on the last day you drove was below 0. How about setting yourself a target of scoring above 5 for the rest of the week to increase your chances of earning rewards?",
                           @"Explaining your driving score",
                           @"Your %@ score on the last day you drove was below 0. How about setting yourself a target of scoring above 5 for the rest of the week to increase your chances of earning rewards?"];
            mScores = @[@(5),@(2.2),@(-2.7),@(1.4),@(0.0)];
            break;
        case 2:
            arrayTexts = @[@"Improving your usage score will help to reduce your chances of being in an accident, and increase your rewards. See our tips for improving your usage score.",
                           @"Your %@ score for the last day you drove didn’t quite reach 5. How about setting yourself a target of scoring above 5 for the rest of the week?",
                           @"Your %@ score on the last day you drove was below 0. How about setting yourself a target of scoring above 5 for the rest of the week to increase your chances of earning rewards?",
                           @"Explaining your driving score",
                           @"Your %@ score on the last day you drove was below 0. How about setting yourself a target of scoring above 5 for the rest of the week to increase your chances of earning rewards?"];
            mScores = @[@(-2),@(-1.2),@(-4.7),@(8.4),@(0.0)];
            break;
        case 3:
            arrayTexts = @[@"Improving your usage score will help to reduce your chances of being in an accident, and increase your rewards. See our tips for improving your usage score.",
                           @"Your %@ score for the last day you drove didn’t quite reach 5. How about setting yourself a target of scoring above 5 for the rest of the week?",
                           @"Your %@ score on the last day you drove was below 0. How about setting yourself a target of scoring above 5 for the rest of the week to increase your chances of earning rewards?",
                           @"Explaining your driving score",
                           @"Your %@ score on the last day you drove was below 0. How about setting yourself a target of scoring above 5 for the rest of the week to increase your chances of earning rewards?"];
            mScores = @[@(4),@(-7.2),@(2.7),@(-3.4),@(0.0)];
            break;
        case 4:
            arrayTexts = @[@"Improving your usage score will help to reduce your chances of being in an accident, and increase your rewards. See our tips for improving your usage score.",
                           @"Your %@ score for the last day you drove didn’t quite reach 5. How about setting yourself a target of scoring above 5 for the rest of the week?",
                           @"Your %@ score on the last day you drove was below 0. How about setting yourself a target of scoring above 5 for the rest of the week to increase your chances of earning rewards?",
                           @"Explaining your driving score",
                           @"Your %@ score on the last day you drove was below 0. How about setting yourself a target of scoring above 5 for the rest of the week to increase your chances of earning rewards?"];
            mScores = @[@(9),@(7.2),@(26.7),@(5.4),@(0.0)];
            break;
    }
    
    NSMutableArray *mArray = [[NSMutableArray alloc]init];
    
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        @autoreleasepool
        {
            ScoresDataAux *data = [ScoresDataAux new];
            data.title = array[idx];
            data.text = arrayTexts[idx];
            NSInteger score = [[mScores objectAtIndex:idx]integerValue] ;
            data.value = [NSString stringWithFormat:@"%@",[mScores objectAtIndex:idx]];
            if(score < 0)
            {
                data.color = [UIColor colorWithRed:255/255.0f green:58/255.0f blue:31/255.0f alpha:1];
            }
            else if (score < 5)
            {
                data.color = [UIColor colorWithRed:242/255.0f green:193/255.0f blue:71/255.0f alpha:1];
            }
            else
            {
                data.color = [UIColor colorWithRed:91/255.0f green:206/255.0f blue:124/255.0f alpha:1];
            }
            data.degrees = [self degreesFromValue: score];
            //add the object to the array
            [mArray addObject:data];
        }
    }];
    
    return mArray;
}

/** convenient method to transform value to degrees in radians, ready to use in transform property **/
- (float) degreesFromValue:(NSInteger)score
{
    BOOL negativeScore = score < 0;
    
    float twentyPercent = kAngleTotal / kNumberOfSteps;
    score = negativeScore ? -score : score;
    
    NSInteger total = twentyPercent * score;
    total = negativeScore ? 0-total : total;
    
    return total * M_PI / 180.0;
}

@end

@implementation TipsDataAux

- (NSMutableArray *) contentArray
{
    NSArray *array = @[@"icon_tab_tips_off",
                       @"icon_tab_tips_off",
                       @"icon_tab_tips_off",
                       @"icon_tab_tips_off",
                       @"icon_tab_tips_off"];
    
    NSArray *arrayTexts = @[@"Improving your usage score will help to reduce your chances of being in an accident, and increase your rewards. See our tips for improving your usage score.",
                            @"Your %@ score for the last day you drove didn’t quite reach 5. How about setting yourself a target of scoring above 5 for the rest of the week?",
                            @"Your %@ score on the last day you drove was below 0. How about setting yourself a target of scoring above 5 for the rest of the week to increase your chances of earning rewards?",
                            @"Explaining your driving score",
                            @"Your %@ score on the last day you drove was below 0. How about setting yourself a target of scoring above 5 for the rest of the week to increase your chances of earning rewards?"];
    
    NSMutableArray *mArray = [[NSMutableArray alloc]init];
    
    [array enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        @autoreleasepool
        {
            TipsDataAux *data = [TipsDataAux new];
            data.imgName = array[idx];
            data.text = arrayTexts[idx];
            [mArray addObject:data];
        }
    }];
    
    return mArray;
}

@end
