//
//  AppDelegate.h
//  CollectionViewTest
//
//  Created by Jose Catala on 28/08/2018.
//  Copyright © 2018 Track Global. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

